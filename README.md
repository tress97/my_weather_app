### my_weather_app
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
---

<div align="center">

<img src="https://media.tenor.com/vomUfCcVerQAAAAC/pixelart-japan.gif" />
</div>

This project is a free implementation of a primitive weather application based on Django and OpenMeteo.

### Getting Started

---

> NOTE: All the specified commands are usable for Linux similar systems

1. Clone the repository:
        
        git clone https://gitlab.com/tress97/my_weather_app.git

2. Create your own virtual env (if you need this):

        cd my_weather_app
        python3 -m venv venv
        source venv/bin/activate

3. Then need to install project requirements:

        pip3 install -r requirements.txt

4. In my_weather_app dir need to create environment file:

        cd my_weather_app
        touch .env
        vim .env

    Example of `.env` file:

        SECRET_KEY='your_django_secret_key'
        DEBUG='True_or_False'

5. And finally run a project:

        python3 manage.py runserver

##### Happy use! :D


### Useful links:

---

* [Django Project](https://docs.djangoproject.com/en/4.1/)
* [OpenMeteoPy](https://github.com/m0rp43us/openmeteopy)
* [Open-Meteo](https://open-meteo.com/)
* [Bootstrap](https://icons.getbootstrap.com/)