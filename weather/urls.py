from django.urls import path
from .views import IndexView, DailyView, HourlyView


app_name = 'weather'
urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('location/<str:location_name>/daily/', DailyView.as_view(), name='daily'),
    path('location/<str:location_name>/daily/<str:date>', HourlyView.as_view(), name='hourly'),
]
