from django.views import View
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.templatetags.static import static

from datetime import datetime
from forms.geolocation import LocationForm
from openmeteo_py import Hourly, Daily, Options, OWmanager
from geopy.geocoders import Nominatim

geolocator = Nominatim(user_agent='django-weather-app')


class IndexView(View):
    form_class = LocationForm
    template_name = 'index.html'
    error_template_name = 'error.html'

    def get(self, request):
        form = self.form_class
        return render(request, template_name='index.html', context={'form': form, 'location_name': None})

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            return HttpResponseRedirect(f'/location/{form["name"].data}/daily')

        return render(request, template_name='index.html', context={'form': form, 'location_name': None})


class DailyView(View):
    form_class = LocationForm
    template_name = 'daily.html'
    error_template_name = 'error.html'

    def get(self, request, location_name):
        location = geolocator.geocode(location_name)

        # Check: is location exist
        if location is not None:
            daily = Daily()
            options = Options(location.latitude, location.longitude)
            mgr = OWmanager(
                options,
                daily=daily
                .temperature_2m_min()
                .temperature_2m_max()
                .precipitation_sum()
                .windspeed_10m_max()
                .weathercode()
            )

            # Get weather data and reformat
            mgr_respond = mgr.Jsonify(meteo=mgr.get_data())
            weather_data = reformat_daily(mgr_respond)

            # Check: is dicts empty
            if weather_data:
                return render(
                    request=request,
                    template_name=self.template_name,
                    context={
                        'today_date': datetime.now().strftime('%Y-%m-%d'),
                        'weather_data': weather_data,
                        'form': self.form_class(initial={'name': location_name}),
                        'location_name': location_name
                    }
                )

        return render(
            request=request,
            template_name=self.error_template_name,
            context={
                'form': self.form_class(initial={'name': location_name}),
                'location_name': None
            }
        )


class HourlyView(View):
    form_class = LocationForm
    template_name = 'hourly.html'
    error_template_name = 'error.html'

    def get(self, request, location_name, date):
        location = geolocator.geocode(location_name)

        # Check: is location exist
        if location is not None:
            hourly = Hourly()
            daily = Daily()
            options = Options(location.latitude, location.longitude)
            mgr = OWmanager(
                options,
                hourly=hourly
                .temperature_2m()
                .cloudcover()
                .precipitation()
                .weathercode()
                .windgusts_10m()
                .relativehumidity_2m(),
                daily=daily
                .temperature_2m_min()
                .temperature_2m_max()
                .precipitation_sum()
                .windspeed_10m_max()
                .weathercode()
            )

            # Get weather data and reformat
            mgr_respond = mgr.Jsonify(meteo=mgr.get_data())
            weather_hourly_data = reformat_hourly(mgr_respond, date)
            weather_daily_data = reformat_daily(mgr_respond, date)

            # Check: is dicts empty
            if weather_hourly_data and weather_daily_data:
                return render(
                    request=request,
                    template_name=self.template_name,
                    context={
                        'today_date': datetime.now().strftime('%Y-%m-%d'),
                        'weather_daily_data': weather_daily_data,
                        'weather_hourly_data': weather_hourly_data,
                        'form': LocationForm(initial={'name': location_name}),
                        'location_name': location_name
                    }
                )
        return render(
            request=request,
            template_name=self.error_template_name,
            context={
                'form': LocationForm(initial={'name': location_name}),
                'location_name': None
            }
        )


def reformat_daily(daily_dict: dict, date: str = '') -> dict:
    """
    A common function to format dictionary data for better interaction in templates

    For more information visit Daily Parameter Definition block
    https://open-meteo.com/en/docs

    Parameters
    ----------
    daily_dict : dict
    date : str, optional

    Returns
    ----------
    dict
        formatted daily dictionary
    """

    weather_daily_data: dict = {}
    for daily_iter in daily_dict['daily']['time']:
        if daily_iter.startswith(date):
            weather_daily_data[daily_iter] = {
                'temperature_2m_min': daily_dict['daily']['temperature_2m_min'][daily_iter],
                'temperature_2m_max': daily_dict['daily']['temperature_2m_max'][daily_iter],
                'precipitation_sum': daily_dict['daily']['precipitation_sum'][daily_iter],
                'windspeed_10m_max': daily_dict['daily']['windspeed_10m_max'][daily_iter],
            }
            weather_daily_data[daily_iter]['weather_name'], weather_daily_data[daily_iter]['weather_icon'] = \
                get_weather_name(daily_dict['daily']['weathercode'][daily_iter])

    return weather_daily_data


def reformat_hourly(hourly_dict: dict, date: str = '') -> dict:
    """
    A common function to format dictionary data for better interaction in templates

    For more information visit Hourly Parameter Definition block
    https://open-meteo.com/en/docs

    Parameters
    ----------
    hourly_dict : dict
    date : str, optional

    Returns
    ----------
    dict
        formatted hourly dictionary
    """

    weather_hourly_data: dict = {}

    for time_iter in hourly_dict['hourly']['time']:
        if time_iter.startswith(date):
            time = datetime.fromisoformat(time_iter).strftime('%H:%M')
            weather_hourly_data[time] = {
                'temperature_2m': hourly_dict['hourly']['temperature_2m'][time_iter],
                'cloudcover': hourly_dict['hourly']['cloudcover'][time_iter],
                'precipitation': hourly_dict['hourly']['precipitation'][time_iter],
                'windgusts_10m': hourly_dict['hourly']['windgusts_10m'][time_iter],
                'relativehumidity_2m': hourly_dict['hourly']['relativehumidity_2m'][time_iter],
            }
            weather_hourly_data[time]['weather_name'], weather_hourly_data[time]['weather_icon'] = \
                get_weather_name(hourly_dict['hourly']['weathercode'][time_iter])

    return weather_hourly_data


def get_weather_name(weather_code: str) -> tuple:
    """
    Function to get additional data using a weather code

    For more information visit Weather variable documentation block
    https://open-meteo.com/en/docs

    Parameters
    ----------
    weather_code : str

    Returns
    ----------
    tuple
        (str, static) - weather name and appropriate static icon file
    """

    code = int(weather_code)
    if code == 0:
        return 'Clear sky', static('icons/sun-fill.svg')
    elif 1 <= code <= 3:
        return 'Mainly clear', static('icons/cloud-sun-fill.svg')
    elif 45 <= code <= 48:
        return 'Fog', static('icons/cloud-fog2-fill.svg')
    elif 51 <= code <= 67:
        return 'Drizzle', static('icons/cloud-drizzle-fill.svg')
    elif 71 <= code <= 86:
        return 'Snow', static('icons/cloud-snow-fill.svg')
    elif code <= 99:
        return 'Thunderstorm', static('icons/tropical-storm.svg')
