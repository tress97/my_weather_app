from django import forms


class LocationForm(forms.Form):
    name = forms.CharField(
        label='',
        widget=forms.TextInput(attrs={'placeholder': ' ...type your location... '})
    )
